## Website CV

This is a small individual project I have made to not only learn but also showcase some of my skills in terms of web design and programming. The website will contain a digital version of my CV and in the future, small fun web-based projects to showcase mathematical skills and javascript. I may make the projects up myself or write some solutions to problems found on https://projecteuler.net/.

Note: I am currently an Apprentice Software Developer at CGI and I am more familiar with backend Java rather than frontend but I am keen to learn about frontend technology too in my free time.
